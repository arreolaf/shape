/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.shape;

/**
 *
 * @author mymac
 */
public class Rectangle {

    private double height;
    private double width;
    
    Rectangle(){
        
    }

    Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    public double getHeight() {
        return this.height;
    }

    public double getWidth() {
        return this.width;
    }
    
    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
       this.width = width;
    }
    
    public double getArea(){
        return this.height * this.width;
    }

}
